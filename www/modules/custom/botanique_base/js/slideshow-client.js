/**
 * @file
 * Slideshow behaviors.
 */

(function ($, Drupal) {

    'use strict';

    var interval;

    Drupal.behaviors.botanique = {};

    /**
     * Attach slideshow behaviors
     */
    Drupal.behaviors.botanique_slideshow = {
        attach: function (context) {
            var $context = $(context);

            // Connect to the socket
            var conn = new ab.Session('ws://dev.local:8080',
                function() {
                    conn.subscribe(slideId, function(topic, data) {
                        Drupal.behaviors.botanique.updateSlides($context, data)
                    });
                },
                function() {
                    console.warn('WebSocket connection closed');
                },
                {'skipSubprotocolCheck': true}
            );

            interval = setInterval(function() {
                Drupal.behaviors.botanique.nextSlide($context);
            }, 5000);
        }
    };

    /**
     * Replace the slideshow slides
     *
     * @param $context
     * @param data
     */
    Drupal.behaviors.botanique.updateSlides = function ($context, data) {
                // We don't know what response.data contains: it might be a string of text
        // without HTML, so don't rely on jQuery correctly interpreting
        // $(response.data) as new HTML rather than a CSS selector. Also, if
        // response.data contains top-level text nodes, they get lost with either
        // $(response.data) or $('<div></div>').replaceWith(response.data).
        var $new_content_wrapped = $('<div></div>').html(data.slides);
        var $new_content = $new_content_wrapped.contents();

        // For legacy reasons, the effects processing code assumes that
        // $new_content consists of a single top-level element. Also, it has not
        // been sufficiently tested whether attachBehaviors() can be successfully
        // called with a context object that includes top-level text nodes.
        // However, to give developers full control of the HTML appearing in the
        // page, and to enable Ajax content to be inserted in places where <div>
        // elements are not allowed (e.g., within <table>, <tr>, and <span>
        // parents), we check if the new content satisfies the requirement
        // of a single top-level element, and only use the container <div> created
        // above when it doesn't. For more information, please see
        // https://www.drupal.org/node/736066.
        if ($new_content.length !== 1 || $new_content.get(0).nodeType !== 1) {
            $new_content = $new_content_wrapped;
        }

        // Preload the html and then add
        $context.find('#slideshow').html($new_content.get(0));

        clearInterval(interval);

        interval = setInterval(function() {
            Drupal.behaviors.botanique.nextSlide($context);
        }, 5000);
    }

    /**
     * Show the next slide
     *
     * @param $context
     */
    Drupal.behaviors.botanique.nextSlide = function ($context) {
        var currentSlide = $context.find('.slide:visible');
        var nextSlide = currentSlide.next();

        // Hide all slides
        $context.find('.slide').hide();

        // Show the next slide
        if (!nextSlide.length) {
            currentSlide.parent().find('.slide').first().show();
        } else {
            nextSlide.show();
        }
    }

})(jQuery, Drupal);
