<?php

use Drupal\botanique_base\Pusher;
use Drupal\Core\DrupalKernel;
use React\Socket\Server;
use React\ZMQ\Context;
use Symfony\Component\HttpFoundation\Request;

// Bootstrap Drupal
chdir('../../..');

$autoloader = require_once 'autoload.php';

$kernel = DrupalKernel::createFromRequest(Request::createFromGlobals(), $autoloader, 'prod');
$kernel->boot();
$container = $kernel->getContainer();

$loop   = React\EventLoop\Factory::create();
$pusher = new Pusher();

// Listen for the web server to make a ZeroMQ push after an ajax request
$context = new Context($loop);

$pull = $context->getSocket(ZMQ::SOCKET_PULL);
$pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
$pull->on('message', [$pusher, 'onSlideshow']);

// Set up our WebSocket server for clients wanting real-time updates
$webSock = new Server($loop);
$webSock->listen(8080, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
$webServer = new Ratchet\Server\IoServer(
  new Ratchet\Http\HttpServer(
    new Ratchet\WebSocket\WsServer(
      new Ratchet\Wamp\WampServer(
        $pusher
      )
    )
  ),
  $webSock
);

// Run
$loop->run();