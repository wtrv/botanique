<?php

namespace Drupal\botanique_base;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

/**
 * Class Pusher
 * @package Drupal\botanique_base
 */
class Pusher implements WampServerInterface {

  /**
   * A lookup of all the slideshows clients have subscribed to
   */
  protected $subscribedSlideshows = [];

  /**
   * {@inheritdoc}
   */
  public function onSubscribe(ConnectionInterface $conn, $topic) {
    $this->subscribedSlideshows[$topic->getId()] = $topic;
  }

  /**
   * {@inheritdoc}
   */
  public function onUnSubscribe(ConnectionInterface $conn, $topic) {

  }

  /**
   * {@inheritdoc}
   */
  public function onOpen(ConnectionInterface $conn) {

  }

  /**
   * {@inheritdoc}
   */
  public function onClose(ConnectionInterface $conn) {

  }

  /**
   * {@inheritdoc}
   */
  public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
    // In this application if clients send data it's because the user hacked around in console
    $conn->close();
  }

  /**
   * {@inheritdoc}
   */
  public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
    // In this application if clients send data it's because the user hacked around in console
    $conn->close();
  }

  /**
   * {@inheritdoc}
   */
  public function onError(ConnectionInterface $conn, \Exception $e) {

  }

  /**
   * @param string
   *  Callback receiving json string from ZeroMQ
   */
  public function onSlideshow($data) {
    $slideshow = json_decode($data, TRUE);

    // If the lookup slideshow object isn't set there is no one to publish to
    if (!isset($this->subscribedSlideshows['slideshow:'.$slideshow['id']])) {
      return;
    }

    $topic = $this->subscribedSlideshows['slideshow:'.$slideshow['id']];

    // Send the data to all the clients subscribed to that slideshow
    $topic->broadcast($slideshow);
  }


}
