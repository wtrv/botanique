<?php

namespace Drupal\botanique_base\EventSubscriber;

use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class NodeModalEventSubscriber
 * @package Drupal\cmp_base\EventSubscriber
 */
class RedirectEventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * RedirectEventSubscriber constructor.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $routeMatch
   */
  public function __construct(CurrentRouteMatch $routeMatch, RequestStack $requestStack) {
    $this->routeMatch = $routeMatch;
    $this->requestStack = $requestStack;
  }

  /**
   * Redirect to admin content listing for slides
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   */
  public function redirect(GetResponseEvent $event) {
    $routeName = $this->routeMatch->getRouteName();

    // Slide detail
    if ($routeName === 'entity.node.canonical') {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $this->routeMatch->getParameter('node');

      if (!empty($node) && $node instanceOf NodeInterface && $node->bundle() === 'slide') {
        $url = Url::fromRoute('system.admin_content');
        $event->setResponse(new RedirectResponse($url->toString()));
        $this->requestStack->getCurrentRequest()->query->remove('destination');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirect'];

    return $events;
  }
}
