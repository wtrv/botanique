<?php
use Drupal\image\Entity\ImageStyle;

/**
 * Preprocess the slide
 * @param $variables
 */
function template_preprocess_botanique_base_slide(&$variables) {
  /** @var \Drupal\node\NodeInterface $slide */
  $slide = $variables['slide'];
  $variables['label'] = $slide->label();

  if ($slide->hasField('field_image') && !empty($slide->field_image)) {
    /** @var \Drupal\file\Entity\File $image */
    $image = $slide->field_image->entity;

    if ($image) {
      $variables['image'] = ImageStyle::load('slideshow_image')->buildUrl($image->getFileUri());
    }
  }

  $variables['#cache']['tags'] = $slide->getCacheTags();
}