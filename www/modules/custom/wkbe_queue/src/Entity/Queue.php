<?php

namespace Drupal\wkbe_queue\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Queue entity.
 *
 * @ConfigEntityType(
 *   id = "wkbe_queue",
 *   label = @Translation("Queue"),
 *   handlers = {
 *     "list_builder" = "Drupal\wkbe_queue\QueueListBuilder",
 *     "form" = {
 *       "add" = "Drupal\wkbe_queue\Form\QueueForm",
 *       "edit" = "Drupal\wkbe_queue\Form\QueueForm",
 *       "delete" = "Drupal\wkbe_queue\Form\QueueDeleteForm"
 *     },
 *   },
 *   config_prefix = "wkbe_queue",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/wkbe_queue/{wkbe_queue}",
 *     "add-form" = "/admin/structure/wkbe_queue/add",
 *     "edit-form" = "/admin/structure/wkbe_queue/{wkbe_queue}/edit",
 *     "delete-form" = "/admin/structure/wkbe_queue/{wkbe_queue}/delete",
 *     "collection" = "/admin/structure/wkbe_queue",
 *     "sort" = "/admin/structure/wkbe_queue/{wkbe_queue}/sort"
 *   }
 * )
 */

class Queue extends ConfigEntityBase implements QueueInterface {

  /**
   * The Queue ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Queue label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this queue.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }
}
