<?php

/**
 * @file
 * Contains \Drupal\wkbe_queue\QueueHelper service.
 */

namespace Drupal\wkbe_queue;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\wkbe_queue\Entity\QueueInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QueueHelper.
 * Provides a service with helpful methods for performing Queue operations
 *
 * @package Drupal\wkbe_queue
 */
class QueueHelper {

  /**
   * @var Database $database
   */
  protected $database;

  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * @var string
   *  The queue table
   */
  private $table = 'wkbe_queue';

  /**
   * QueueHelper constructor.
   * @param Connection $database
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Merges a single record into the wkbe_queue table with an optional weight
   *
   * @param EntityInterface $entity
   * @param string $queue
   * @param null $weight
   * @return StatementInterface|int|null
   */
  public function mergeRecord(EntityInterface $entity, $queue, $weight = NULL) {
    $fields = [
      'label' => $entity->label()
    ];

    if ($weight) {
      $fields['weight'] = (int) $weight;
    }

    $this->invalidateCacheTags();

    return $this->database->merge($this->table)
      ->key(['queue' => $queue, 'entity_id' => $entity->id(), 'storage' => $entity->getEntityTypeId(), 'bundle' => $entity->bundle()])
      ->fields($fields)
      ->execute();
  }

  /**
   * Updates the weight for a single record in the wkbe_queue table
   *
   * @param $queue
   * @param $entity_id
   * @param $storage
   * @param $bundle
   * @param $weight
   * @return StatementInterface|int|null
   * @throws \Exception
   */
  public function updateWeight($queue, $entity_id, $storage, $bundle, $weight) {
    $this->invalidateCacheTags();

    return $this->database->merge($this->table)
      ->key(['queue' => $queue, 'entity_id' => $entity_id, 'storage' => $storage, 'bundle' => $bundle])
      ->fields(['weight' => $weight])
      ->execute();
  }

  /**
   * Merges records into the wkbe_queue table
   *
   * @param EntityInterface $entity
   * @param array $queues
   * @param bool $clean
   *  Indicates if the stale entries need to be deleted
   * @throws \Exception
   */
  public function mergeRecords(EntityInterface $entity, array $queues, $clean = FALSE) {
    $this->invalidateCacheTags();

    foreach ($queues as $queue) {
      $this->database->merge($this->table)
        ->key(['queue' => $queue, 'entity_id' => $entity->id(), 'storage' => $entity->getEntityTypeId(), 'bundle' => $entity->bundle()])
        ->fields(['label' => $entity->label()])
        ->execute();
    }

    // Remove stale records
    if ($clean) {
      $query = $this->database->delete($this->table);
        if (!empty($queues)) {
          $query->condition('queue', $queues, 'NOT IN');
        }

      $query->condition('entity_id', $entity->id())
        ->condition('storage', $entity->getEntityTypeId())
        ->condition('bundle', $entity->bundle())
        ->execute();
    }
  }

  /**
   * Get the queues for the given entity
   *
   * @param EntityInterface $entity
   * @return array
   */
  public function getQueuesForEntity(EntityInterface $entity) {
    $query = $this->database->select($this->table)->fields($this->table, ['queue']);
    $query->condition('entity_id', $entity->id())
          ->condition('storage', $entity->getEntityTypeId())
          ->condition('bundle', $entity->bundle());

    $queues = $query->execute()->fetchCol();

    return $queues;
  }

  /**
   * Delete the queue entries for a given entity
   *
   * @param EntityInterface $entity
   * @return int
   */
  public function deleteEntriesForEntity(EntityInterface $entity) {
    return $this->database->delete($this->table)
      ->condition('entity_id', $entity->id())
      ->condition('storage', $entity->getEntityTypeId())
      ->condition('bundle', $entity->bundle())
      ->execute();
  }

  /**
   * Delete the queue entries for a given queue
   *
   * @param QueueInterface $queue
   * @return int
   */
  public function deleteEntriesForQueue(QueueInterface $queue) {
    $this->invalidateCacheTags();

    return $this->database->delete($this->table)
      ->condition('queue', $queue->id())
      ->execute();
  }

  /**
   * Delete a single queue entrie for a given entity
   *
   * @param EntityInterface $entity
   * @param QueueInterface $queue
   * @return int
   */
  public function deleteEntryForEntityAndQueue(EntityInterface $entity, QueueInterface $queue) {
    $this->invalidateCacheTags();
    
    return $this->database->delete($this->table)
      ->condition('entity_id', $entity->id())
      ->condition('storage', $entity->getEntityTypeId())
      ->condition('bundle', $entity->bundle())
      ->condition('queue', $queue->id())
      ->execute();
  }

  /**
   * Get all entities for a given queue
   *
   * @param QueueInterface $queue
   * @param bool $load
   * @param bool $limit
   * @return array
   */
  public function getEntitiesForQueue(QueueInterface $queue, $limit = FALSE, $load = TRUE) {
    $query = $this->database->select($this->table, 'q')
        ->fields('q');

    if ($limit) {
      $query->range(0, $limit);
    }

    $query->condition('q.queue', $queue->id())
        ->orderBy('q.weight', 'ASC')
        ->orderBy('q.label', 'ASC');

    $entries = $query->execute()->fetchAll();

    return $this->loadEntries($entries, $load);
  }

  /**
   * Get all entities for a single queue by queue id
   *
   * @param string $queue
   * @param bool $load
   * @param bool $limit
   * @return array
   */
  public function getEntitiesForQueueName($queue, $limit = FALSE, $load = TRUE) {
    $query = $this->database->select($this->table, 'q')
      ->fields('q');

    if ($limit) {
      $query->range(0, $limit);
    }

    $query->condition('q.queue', $queue)
      ->orderBy('q.weight', 'ASC')
      ->orderBy('q.label', 'ASC');

    $entries = $query->execute()->fetchAll();

    return $this->loadEntries($entries, $load);
  }

  /**
   * Get all entities for multiple queues
   *
   * @param array $queues
   * @param bool $load
   * @param bool $limit
   * @return array
   */
  public function getEntitiesForQueues($queues, $limit = FALSE, $load = TRUE) {
    if (empty($queues)) {
      return [];
    }

    $query = $this->database->select($this->table, 'q')
      ->fields('q');

    if ($limit) {
      $query->range(0, $limit);
    }

    $query->condition('q.queue', $queues, 'IN')
      ->orderBy('q.weight', 'ASC')
      ->orderBy('q.label', 'ASC');

    $entries = $query->execute()->fetchAll();

    return $this->loadEntries($entries, $load);
  }

  /**
   * Helper function for load entities
   * @param $entries
   * @param bool $load
   * @return mixed
   */
  private function loadEntries($entries, $load = TRUE) {
    $entities = [];
    $storages = [];

    foreach ($entries as $entry) {
      if (!$load) {
        $entities[$entry->storage . '_' . $entry->entity_id] = $entry;
      }else {
        if (empty($storages[$entry->storage])) {
          $storages[$entry->storage] = $this->entityTypeManager->getStorage($entry->storage);
        }

        $entities[$entry->storage . '_' . $entry->entity_id] = $storages[$entry->storage]->load($entry->entity_id);
      }
    }

    return $entities;
  }

  /**
   * Helper function for invalidating the queue cache tags
   */
  private function invalidateCacheTags(){
    Cache::invalidateTags(['wkbe:queue']);
  }
}