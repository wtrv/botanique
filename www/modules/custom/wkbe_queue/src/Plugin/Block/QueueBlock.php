<?php

namespace Drupal\wkbe_queue\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\wkbe_queue\Entity\Queue;
use Drupal\wkbe_queue\QueueHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'QueuBlock' block plugin.
 *
 * @Block(
 *   id = "queue_block",
 *   admin_label = @Translation("Queue block"),
 *   deriver = "Drupal\wkbe_queue\Plugin\Derivative\QueueBlock"
 * )
 */

class QueueBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * @var QueueHelper $queueHelper
   */
  protected $queueHelper;

  /**
   * Creates a QueueBlock instance.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param array $plugin_definition
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, QueueHelper $queue_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->queueHelper = $queue_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('wkbe_queue.queue_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    $form['view_mode'] = [
      '#type' => 'textfield',
      '#title' => t('View mode'),
      '#default_value' => isset($config['view_mode']) ? $config['view_mode'] : 'teaser',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('view_mode', $form_state->getValue('view_mode'));
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    $queue = Queue::load($this->getDerivativeId());
    $config = $this->getConfiguration();

    $build = [
      '#theme' => 'wkbe_queue_list',
      '#entities' => $this->queueHelper->getEntitiesForQueue($queue),
      '#view_mode' => !empty($config['view_mode']) ? $config['view_mode'] : 'teaser',
    ];

    return $build;
  }
}