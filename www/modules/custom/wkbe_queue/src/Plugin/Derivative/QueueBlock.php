<?php

/**
 * @file
 * Contains \Drupal\wkbe_queue\Plugin\Derivative\QueueBlock.
 */

namespace Drupal\wkbe_queue\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\wkbe_queue\Entity\Queue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for wkbe queues.
 *
 * @see \Drupal\wkbe_queue\Plugin\Block\QueueBlock
 */
class QueueBlock extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Constructs new QueueBlock.
   *
   */
  public function __construct() {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $queues = Queue::loadMultiple();

    /** @var Queue $queue */
    foreach ($queues as $queue) {
      $this->derivatives[$queue->id()] = $base_plugin_definition;
      $this->derivatives[$queue->id()]['admin_label'] = t('Queue block: ') . $queue->label();
    }

    return $this->derivatives;
  }
}