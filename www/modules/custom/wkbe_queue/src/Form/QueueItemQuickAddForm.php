<?php

/**
 * @file
 * Contains \Drupal\wkbe_queue\Form\QueueSortForm.
 */

namespace Drupal\wkbe_queue\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Query\Merge;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\wkbe_queue\Entity\Queue;
use Drupal\wkbe_queue\Entity\QueueInterface;
use Drupal\wkbe_queue\QueueHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Form for quick adding an entity to a Queue.
 *
 * @ingroup wkbe_queue
 */
class QueueItemQuickAddForm extends FormBase {

  /**
   * @var StateInterface $state
   */
  protected $state;

  /**
   * @var EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var EntityTypeBundleInfoInterface $entityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * @var QueueHelper $queueHelper
   */
  protected $queueHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, QueueHelper $queue_helper) {
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->queueHelper = $queue_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('wkbe_queue.queue_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wkbe_queue_quick_add_form';
  }

  /**
   * Form constructor.
   *
   * Display a tree of all the entities in this queue
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @param QueueInterface $wkbe_queue
   * @return array The form structure.
   * The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, QueueInterface $wkbe_queue = NULL) {
    $form['add_entity'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Quick add entity'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $entity_types = $this->entityTypeBundleInfo->getAllBundleInfo();
    $definitions = $this->entityTypeManager->getDefinitions();

    $enabled_bundles = array_filter($this->state->get('wkbe_queue.enabled_bundles'));
    $options = [];

    foreach ($enabled_bundles as $entity_type => $bundles) {
      foreach($bundles as $bundle) {
        $options[$definitions[$entity_type]->getLabel()->render()][$entity_type.':'.$bundle] = $entity_types[$entity_type][$bundle]['label'];
      }
    }

    $form['add_entity']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of item to reference'),
      '#description' => $this->t('Select the type of the entity you want to add.'),
      '#options' => $options,
      '#default_value' => NULL,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => 'Drupal\wkbe_queue\Form\QueueItemQuickAddForm::autocompleteForm',
        'wrapper' => 'my-wrapper',
        'event' => 'change',
        'method' => 'html',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#suffix' => '<div id="my-wrapper"></div>',
    ];

    if ($type = $form_state->getValue('type')) {
      list($entity_type, $bundle) = explode(':', $type);

      $form['add_entity']['entity_select']['entity'] = [
        '#type' => 'entity_autocomplete',
        '#title' => t('Select entity'),
        '#target_type' => $entity_type,
        '#default_value' => NULL,
        '#required' => TRUE,
        '#maxlength' => 255,
        '#selection_settings' => [
          'target_bundles' => [$bundle],
        ],
      ];


      $form['add_entity']['entity_select']['actions']['#type'] = 'actions';
      $form['add_entity']['entity_select']['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Add entity'),
        '#button_type' => 'primary',
      );
    }

    return $form;
  }

  public function autocompleteForm(&$form, FormStateInterface $form_state) {
    $bundle = $form_state->getValue('type');

    if ($bundle) {
      return $form['add_entity']['entity_select'];
    }

    return ['#markup' => ''];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $build_info = $form_state->getBuildInfo();

    $entity_id = $form_state->getValue('entity');
    $type = $form_state->getValue('type');

    /** @var Queue $current_queue */
    $current_queue = $build_info['args'][0];

    if ($current_queue) {
      list($entity_type, $bundle) = explode(':', $type);
      $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);

      // Add to queue
      $merge = $this->queueHelper->mergeRecord($entity, $current_queue->id());
      if ($merge === Merge::STATUS_INSERT) {
        drupal_set_message($this->t('Added %label to the queue.', ['%label' => $entity->label()]));
      }elseif ($merge === Merge::STATUS_UPDATE) {
        drupal_set_message($this->t('%label is already in the queue.', ['%label' => $entity->label()]), 'warning');
      }

      Cache::invalidateTags(['wkbe:queue']);
    }
  }

}

