<?php

namespace Drupal\wkbe_queue\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wkbe_queue\Entity\Queue;

/**
 * Class QueueForm.
 *
 * @package Drupal\wkbe_queue\Form
 */
class QueueForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var Queue $wkbe_queue */
    $wkbe_queue = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $wkbe_queue->label(),
      '#description' => $this->t("Label for the Queue."),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $wkbe_queue->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\wkbe_queue\Entity\Queue::load',
      ),
      '#disabled' => !$wkbe_queue->isNew(),
    );

    $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $wkbe_queue->getDescription(),
      '#description' => t('This text will be displayed on the <em>Queues</em> page.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $wkbe_queue = $this->entity;
    $status = $wkbe_queue->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Queue.', [
          '%label' => $wkbe_queue->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Queue.', [
          '%label' => $wkbe_queue->label(),
        ]));
    }
    $form_state->setRedirectUrl($wkbe_queue->urlInfo('collection'));
  }

}
