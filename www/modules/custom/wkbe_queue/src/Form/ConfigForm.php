<?php

/**
 * @file
 * Contains \Drupal\wkbe_queue\Form\ConfigForm.
 */

namespace Drupal\wkbe_queue\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wkbe_common\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm.
 *
 * wkbe_queue configuration form.
 *
 * @package Drupal\wkbe_entitysort\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * @var EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var EntityTypeBundleInfoInterface $entityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    parent::__construct($config_factory, $state);


    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wkbe_queue_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getStatePrefix() {
    return 'wkbe_queue';
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName() {
    return 'wkbe_queue.settings';
  }

  /**
   * @inheritdoc
   */
  protected function getEditableConfigNames() {
    $config_names = parent::getEditableConfigNames();

    return $config_names;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $default_values = !empty($this->getSetting('enabled_bundles', ConfigFormBase::CONFIG_TYPE_CONFIG)) ? $this->getSetting('enabled_bundles', ConfigFormBase::CONFIG_TYPE_CONFIG) : [];

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Enabled entity types'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $entity_types = $this->entityTypeBundleInfo->getAllBundleInfo();
    $definitions = $this->entityTypeManager->getDefinitions();
    foreach (array_keys($entity_types) as $entity_type) {
      $entity_class = 'Drupal\Core\Entity\ContentEntityTypeInterface';

      if (is_subclass_of($definitions[$entity_type], $entity_class)) {
        $options = [];
        foreach ($entity_types[$entity_type] as $key => $type) {
          $options[$key] = $type['label'];
        }

        $form['settings'][$entity_type] = [
          '#type' => 'checkboxes',
          '#title' => $definitions[$entity_type]->getLabel(),
          '#options' => $options,
          '#default_value' => !empty($default_values[$entity_type]) ? $default_values[$entity_type] : [],
          '#required' => FALSE,
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_types = $this->entityTypeBundleInfo->getAllBundleInfo();
    $enabled_bundles = [];

    foreach (array_keys($entity_types) as $entity_type) {
      if ($values = $form_state->getValue($entity_type)) {
        $enabled_bundles[$entity_type] = array_filter(array_values($values));
      }
    }

    $this->setSettings([
      'enabled_bundles' => $enabled_bundles,
    ], ConfigFormBase::CONFIG_TYPE_CONFIG);
  }

}
