<?php

namespace Drupal\wkbe_queue\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wkbe_queue\Entity\QueueInterface;
use Drupal\wkbe_queue\QueueHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to remove Queue items from queue.
 */
class QueueItemRemoveForm extends ConfirmFormBase {

  /**
   * @var EntityInterface $entity
   */
  protected $entity;

  /**
   * @var QueueInterface $queue
   */
  protected $queue;

  /**
   * @var QueueHelper $queueHelper
   */
  protected $queueHelper;

  /**
   * @var EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(QueueHelper $queue_helper, EntityTypeManagerInterface $entity_type_manager) {
    $this->queueHelper = $queue_helper;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('wkbe_queue.queue_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wkbe_queue_remove_queue_item_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function setQueue(QueueInterface $queue) {
    $this->queue = $queue;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueue() {
    return $this->queue;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %label from the %queue queue?', ['%label' => $this->entity->label(), '%queue' => $this->queue->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.wkbe_queue.sort', ['wkbe_queue' => $this->queue->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, QueueInterface $wkbe_queue = NULL, int $entity_id = NULL, string $entity_storage = NULL, string $bundle = NULL) {
    $this->queue = $wkbe_queue;
    $this->entity = $this->entityTypeManager->getStorage($entity_storage)->load($entity_id);

    if (!$this->entity) {
      $this->redirect($this->getCancelUrl());
    }

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->queueHelper->deleteEntryForEntityAndQueue($this->entity, $this->queue);

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
