<?php

/**
 * @file
 * Contains \Drupal\wkbe_queue\Form\QueueSortForm.
 */

namespace Drupal\wkbe_queue\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\wkbe_queue\Entity\QueueInterface;
use Drupal\wkbe_queue\QueueHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;


/**
 * Form controller for sorting queues.
 *
 * @ingroup wkbe_queue
 */
class QueueSortForm extends FormBase {

  /**
   * @var StateInterface $state
   */
  protected $state;

  /**
   * @var EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var EntityTypeBundleInfoInterface $entityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * @var Connection $database
   */
  protected $database;

  /**
   * @var QueueHelper $queueHelper
   */
  protected $queueHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, Connection $database, QueueHelper $queueHelper) {
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->database = $database;
    $this->queueHelper = $queueHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('database'),
      $container->get('wkbe_queue.queue_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wkbe_queue_sort';
  }

  /**
   * Form constructor.
   *
   * Display a tree of all the entities in this queue
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @param QueueInterface $wkbe_queue
   * @return array The form structure.
   * The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, QueueInterface $wkbe_queue = NULL) {
    $form['sort'] = [
      '#type' => 'table',
      '#header' => [$this->t('Title'), $this->t('Type'), $this->t('Bundle'), $this->t('Operations'),  $this->t('Weight')],
      '#empty' => $this->t('There are no available items in this queue.'),
      '#attributes' => [
        'id' => 'entity-list',
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'entity-weight',
        ],
      ],
    ];

    if (!$wkbe_queue) {
      return $form;
    }

    // Get all entities for this queue
    $options = $this->queueHelper->getEntitiesForQueue($wkbe_queue, FALSE, FALSE);

    // Types and definitions
    $entity_types = $this->entityTypeBundleInfo->getAllBundleInfo();
    $definitions = $this->entityTypeManager->getDefinitions();

    // Build the table rows and columns.
    foreach ($options as $delta => $item) {
      $id_parts = [
        $item->queue,
        $item->entity_id,
        $item->storage,
        $item->bundle,
      ];

      $item_id = implode(':', $id_parts);

      // TableDrag: Mark the table row as draggable.
      $form['sort'][$item_id]['#attributes']['class'][] = 'draggable';
      // TableDrag: Sort the table row according to its existing/configured weight.
      $form['sort'][$item_id]['#weight'] = $item->weight;

      $form['sort'][$item_id]['label'] = [
        '#plain_text' => $item->label,
      ];

      $form['sort'][$item_id]['storage'] = [
        '#plain_text' => !empty($definitions[$item->storage]->getLabel()) ? $definitions[$item->storage]->getLabel() : NULL,
      ];

      $form['sort'][$item_id]['bundle'] = [
        '#plain_text' => !empty($entity_types[$item->storage][$item->bundle]['label']) ? $entity_types[$item->storage][$item->bundle]['label'] : NULL,
      ];

      $form['sort'][$item_id]['operations'] = [
        '#type' => 'dropbutton',
        '#links' => array(
          'remove' => array(
            'title' => $this->t('Remove'),
            'url' => Url::fromRoute('wkbe_queue.remove_queue_item', ['wkbe_queue' => $wkbe_queue->id(), 'entity_id' => $item->entity_id, 'entity_storage' => $item->storage, 'bundle' => $item->bundle]),
          ),
        ),
      ];

      // TableDrag: Weight column element.
      $form['sort'][$item_id]['weight'] = [
        '#type' => 'weight',
        '#delta' => count($options),
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $item->weight,
        '#attributes' => [
          'class' => ['entity-weight'],
        ],
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $weights = $form_state->getValue('sort');
    $cachetags = [];


    foreach ($weights as $item_id => $item) {
      list($queue, $entity_id, $storage, $bundle) = explode(':', $item_id);
      $this->queueHelper->updateWeight($queue, $entity_id, $storage, $bundle, $item['weight']);

      $cachetags[] = $storage.':'.$entity_id;
    }

    // Clear cache for affected nodes
    Cache::invalidateTags($cachetags);
    drupal_set_message($this->t('The changes have been saved.'));
  }

}

