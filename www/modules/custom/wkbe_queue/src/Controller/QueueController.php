<?php

/**
 * @file
 * Contains \Drupal\wkbe_queue\Controller\QueueController.
 */

namespace Drupal\wkbe_queue\Controller;

use Drupal\wkbe_queue\Entity\QueueInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class QueueController.
 */
class QueueController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * Title callback for queue sort form
   * @param QueueInterface $wkbe_queue
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function sortFormTitle(QueueInterface $wkbe_queue) {
    return $this->t('Sort entities in %queue', ['%queue' => $wkbe_queue->label()]);
  }

  /**
   * Title callback for queue edit form
   * @param QueueInterface $wkbe_queue
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function editFormTitle(QueueInterface $wkbe_queue) {
    return $this->t('Edit %queue', ['%queue' => $wkbe_queue->label()]);
  }
  
  /**
   * Page callback for a Queue sort page
   * @param QueueInterface $wkbe_queue
   * @return array
   */
  public function sortQueue(QueueInterface $wkbe_queue) {
    return [
      $this->formBuilder()->getForm('\Drupal\wkbe_queue\Form\QueueItemQuickAddForm', $wkbe_queue),
      $this->formBuilder()->getForm('\Drupal\wkbe_queue\Form\QueueSortForm', $wkbe_queue),
    ];
  }

}
