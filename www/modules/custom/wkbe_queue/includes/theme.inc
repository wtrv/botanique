<?php
/**
 * @file
 * Theme / preprocess functions for wkbe_queue.
 */

/**
 * Preprocess a queue list.
 */
function template_preprocess_wkbe_queue_list(&$variables) {
  $variables['items'] = [];

  $entity_type_manager = \Drupal::entityTypeManager();
  $render_controllers = [];

  /** @var \Drupal\Core\Entity\EntityInterface $entity */
  foreach ($variables['entities'] as $entity) {

    $entity_type = $entity->getEntityTypeId();
    if (!isset($render_controllers[$entity_type])) {
      $render_controllers[$entity_type] = $entity_type_manager->getViewBuilder($entity_type);
    }

    $variables['items'][] = $render_controllers[$entity_type]->view($entity, $variables['view_mode']);
  }
}